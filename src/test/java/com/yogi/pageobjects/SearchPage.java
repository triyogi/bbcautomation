package com.yogi.pageobjects;

import com.yogi.pageobjectcommands.SearchPageCommands;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class SearchPage {
    public static SearchPageCommands SearchFor(){
        return new SearchPageCommands();
    }
}
