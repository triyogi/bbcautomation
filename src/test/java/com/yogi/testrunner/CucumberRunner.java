package com.yogi.testrunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = {"classpath:cucumber/"}
        ,glue = {"classpath:"}
      //  ,dryRun =true
       //,tags = {"@BbcRegister"}
)
public class CucumberRunner {
}