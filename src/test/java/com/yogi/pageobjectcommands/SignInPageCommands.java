package com.yogi.pageobjectcommands;

import com.yogi.common.lib.Links;
import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class SignInPageCommands extends Links {

    @Override
    public void getLink(String linkType) {
        Driver.getDriver().findElement(By.linkText(linkType)).click();
    }

    public void withDateOfBirth() {
        getLink("Register now");
        Driver.getDriver().findElement(By.id("day-input")).sendKeys("01");
        Driver.getDriver().findElement(By.id("month-input")).sendKeys("01");
        Driver.getDriver().findElement(By.id("year-input")).sendKeys("1983");
        Driver.getDriver().findElement(By.id("submit-button")).click();
    }
}
