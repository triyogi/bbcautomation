package com.yogi.pageobjectcommands;

import com.yogi.common.lib.Links;
import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class NewsPageCommonds  extends Links {

    @Override
    public void getLink(String linkType) {
        Driver.getDriver().findElement(By.linkText(linkType)).click();
    }

    public String verifyLinkText() throws InterruptedException {
        Thread.sleep(2000);
        return Driver.getDriver().findElement(By.xpath("//*[@id=\"brand\"]/span[1]/svg")).getText();
    }
}
