package com.yogi.common.lib;

import com.yogi.common.selenium.Driver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public abstract class Links {

    public abstract void getLink(String linkType);

    public void searchField(){
        Driver.getDriver().findElement(By.id("orb-search-q")).click();
    }

}
