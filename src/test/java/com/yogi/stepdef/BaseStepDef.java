package com.yogi.stepdef;

import org.openqa.selenium.By;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class BaseStepDef {

    String path="application.properties";

    public  String getEnv(String url) {
        Properties prop = new Properties();
        InputStream input=null;
        input = getClass().getClassLoader().getResourceAsStream(path);
        try {
            prop.load(input);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(url);

    }
}
