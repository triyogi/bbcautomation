package com.yogi.stepdef;

import com.yogi.common.selenium.Driver;
import com.yogi.pageobjects.NewsPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class HomePageStepDef {

    @Given("^I am on bbc home page$")
    public void i_am_on_bbc_home_page() throws Throwable {
        Driver.getDriver();
    }

    @When("^I select to \"([^\"]*)\"$")
    public void i_select_to(String linkType) throws Throwable {
        NewsPage.NavigateTo().getLink(linkType);
    }

    @Then("^I should navigate to \"([^\"]*)\"$")
    public void i_should_navigate_to(String linkText) throws Throwable {
        Assert.assertEquals(NewsPage.NavigateTo().verifyLinkText(),linkText);
    }
}
