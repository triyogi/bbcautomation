package com.yogi.stepdef;

import com.yogi.pageobjects.SearchPage;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class SearchPageStepDef {

    @When("^I search for \"([^\"]*)\"$")
    public void i_search_for(String arg1) throws Throwable {
       // SearchPage.SearchFor()
    }

    @Then("^I should get \"([^\"]*)\"$")
    public void i_should_get(String arg1) throws Throwable {
    }
}
