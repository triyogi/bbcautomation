package com.yogi.stepdef;

import com.yogi.pageobjects.SignInPage;
import cucumber.api.java.en.When;

/**
 * Created by Krishan Shukla on 10/09/2017.
 */
public class SignInPageStepDef {
    @When("^I register on bbc app$")
    public void i_register_on_bbc_app() throws Throwable {
        SignInPage.RgisterAs().withDateOfBirth();
    }
}
